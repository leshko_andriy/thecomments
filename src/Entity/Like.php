<?php

namespace App\Entity;

use App\Validator\Constraints as AppAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Like repository
 * @AppAssert\LikeIpConstraints
 * @ORM\Entity(repositoryClass="App\Repository\LikeRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="likes")
 */
class Like
{
    use DateTimeTrait;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(name="ip", type="string", length=25)
     */
    private $ip;

    /**
     * @var Comment
     *
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="likes")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $comment;

    /**
     * @var bool
     *
     * @ORM\Column(name="positive", type="boolean")
     */
    private $positive = true;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return bool|null
     */
    public function getPositive(): ?bool
    {
        return $this->positive;
    }

    /**
     * @param bool $positive
     */
    public function setPositive(bool $positive): void
    {
        $this->positive = $positive;
    }

    /**
     * @return Comment|null
     */
    public function getComment(): ?Comment
    {
        return $this->comment;
    }

    /**
     * @param Comment|null $comment
     */
    public function setComment(?Comment $comment): void
    {
        $this->comment = $comment;
    }
}
