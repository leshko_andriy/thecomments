<?php

namespace App\Entity;

use App\Service\ClientIpService;
use App\Validator\Constraints as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment entity
 *
 * @AppAssert\CommentIpConstraints
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="comments")
 */
class Comment
{
    use DateTimeTrait;

    const COUNT_PARENT_COMMENT = 3;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(name="author", type="string", length=100)
     */
    private $author;

    /**
     * @var string|null
     *
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var bool
     *
     * @ORM\Column(name="new", type="boolean")
     */
    private $new = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @var Company
     *
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="comments")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @var Comment|null
     *
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $parent;

    /**
     * @var Collection|Comment[]
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     */
    protected $children;

    /**
     * @var Collection|Like[]
     *
     * @ORM\OneToMany(targetEntity="Like", mappedBy="comment")
     */
    protected $likes;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(name="ip", type="string", length=25)
     */
    private $ip;

    /**
     * @var int
     *
     * @Assert\Choice({0, 1, 2, 3, 4, 5})
     * @ORM\Column(name="rating", type="integer")
     */
    protected $rating = 0;

    /**
     * @var Collection|Complain[]
     *
     * @ORM\OneToMany(targetEntity="Complain", mappedBy="comment")
     */
    protected $complains;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->children = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->complains = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * @return bool
     */
    public function getNew(): bool
    {
        return $this->new;
    }

    /**
     * @param bool $new
     */
    public function setNew(bool $new): void
    {
        $this->new = $new;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    /**
     * @return Comment|null
     */
    public function getParent(): ?Comment
    {
        return $this->parent;
    }

    /**
     * @param Comment|null $parent
     */
    public function setParent(?Comment $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param Comment $child
     */
    public function addChild(Comment $child): void
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }
    }

    /**
     * @param Comment $child
     */
    public function removeChild(Comment $child): void
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId();
    }

    /**
     * @return int|null
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return Collection|Like[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    /**
     * @param Like $like
     */
    public function addLike(Like $like): void
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setComment($this);
        }
    }

    /**
     * @param Like $like
     */
    public function removeLike(Like $like): void
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getComment() === $this) {
                $like->setComment(null);
            }
        }
    }

    /**
     * @return Collection|Complain[]
     */
    public function getComplains(): Collection
    {
        return $this->complains;
    }

    /**
     * @param Complain $complain
     */
    public function addComplain(Complain $complain): void
    {
        if (!$this->complains->contains($complain)) {
            $this->complains[] = $complain;
            $complain->setComment($this);
        }
    }

    /**
     * @param Complain $complain
     */
    public function removeComplain(Complain $complain): void
    {
        if ($this->complains->contains($complain)) {
            $this->complains->removeElement($complain);
            // set the owning side to null (unless already changed)
            if ($complain->getComment() === $this) {
                $complain->setComment(null);
            }
        }
    }

    /**
     * @return string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->getCreatedAt()->getTimestamp() + 60 * 60 > time() && $this->getIp() === ClientIpService::getIp();
    }

    /**
     * @return Collection|Like[]
     */
    public function getPositiveLikes(): Collection
    {
        return $this->getLikes()->filter(function (Like $like) {
            return $like->getPositive() === true;
        });
    }

    /**
     * @return Collection|Like[]
     */
    public function getNegativeLikes(): Collection
    {
        return $this->getLikes()->filter(function (Like $like) {
            return $like->getPositive() === false;
        });
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->getCompany()->getName();
    }
}
