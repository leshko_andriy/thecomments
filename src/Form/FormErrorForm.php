<?php

namespace App\Form;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Set errors to session flash
 */
class FormErrorForm
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * FormErrorForm constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param FormInterface $form
     */
    public function setOutputError(FormInterface $form): void
    {
        $errors = $form->getErrors(true);
        $errorsToFront = [];
        foreach ($errors as $error) {
            $errorsToFront[] = $error->getMessage();
        }
        $this->session->getFlashBag()->add('formErrors', implode(' ', $errorsToFront));
    }
}