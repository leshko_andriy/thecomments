<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Company;
use App\Form\FormErrorForm;
use App\Form\Type\CommentType;
use App\Service\ClientIpService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Comment controller
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/add/{id}/child/{comment_id}", name="comment_add_child")
     * @ParamConverter("company", options={"id" = "id"})
     * @ParamConverter("comment", options={"id" = "comment_id"})
     * @Route("/add/{id}/", name="comment_add")
     *
     * @param Company $company
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FormErrorForm $formErrorForm
     * @param Comment|null $comment
     * @return Response
     * @throws \Exception
     */
    public function create(
        Company $company,
        Request $request,
        EntityManagerInterface $em,
        FormErrorForm $formErrorForm,
        Comment $comment = null
    ): Response {
        $newComment = new Comment();
        $newComment->setIp(ClientIpService::getIp());
        $newComment->setCompany($company);
        if ($comment !== null) {
            $newComment->setParent($comment);
        }

        $formComment = $this->createForm(CommentType::class, $newComment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $em->persist($newComment);
            $em->flush();
        } else {
            $formErrorForm->setOutputError($formComment);
        }

        return $this->redirectToRoute('company_one', ['id' => $company->getId()]);
    }

    /**
     * @Route("/delete/{id}", name="comment_delete")
     * @Security("comment.isDeleted()")
     *
     * @param Comment $comment
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Comment $comment, EntityManagerInterface $em): Response
    {
        $response = $this->redirectToRoute('company_one', ['id' => $comment->getCompany()->getId()]);
        $em->remove($comment);
        $em->flush();

        return $response;
    }
}
