<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Like;
use App\Form\FormErrorForm;
use App\Form\Type\LikeType;
use App\Service\ClientIpService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Like crud operation
 */
class LikeController extends AbstractController
{
    /**
     * @Route("/like/{id}", name="like_create")
     *
     * @param Comment $comment
     * @param FormErrorForm $formErrorForm
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Exception
     */
    public function create(
        Comment $comment,
        FormErrorForm $formErrorForm,
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $newLike = new Like();
        $newLike->setIp(ClientIpService::getIp());
        $newLike->setComment($comment);

        $formLike = $this->createForm(LikeType::class, $newLike);
        $formLike->handleRequest($request);

        if ($formLike->isSubmitted() && $formLike->isValid()) {
            $em->persist($newLike);
            $em->flush();
        } else {
            $formErrorForm->setOutputError($formLike);
        }

        return $this->redirectToRoute('company_one', ['id' => $comment->getCompany()->getId()]);
    }
}
