<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Complain;
use App\Form\FormErrorForm;
use App\Form\Type\ComplainType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Complain crud operation
 */
class ComplainController extends AbstractController
{
    /**
     * @Route("/complain/{id}", name="complain_create")
     *
     * @param Comment $comment
     * @param FormErrorForm $formErrorForm
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     * @return Response
     * @throws \Exception
     */
    public function create(
        Comment $comment,
        FormErrorForm $formErrorForm,
        Request $request,
        EntityManagerInterface $em,
        SessionInterface $session
    ): Response {
        $newComplain = new Complain();
        $newComplain->setComment($comment);

        $formComplain = $this->createForm(ComplainType::class, $newComplain);
        $formComplain->handleRequest($request);

        if ($formComplain->isSubmitted() && $formComplain->isValid()) {
            $em->persist($newComplain);
            $em->flush();
            $session->getFlashBag()->add('messages', "Complain on comment {$comment->getId()} registered successful");
        } else {
            $formErrorForm->setOutputError($formComplain);
        }

        return $this->redirectToRoute('company_one', ['id' => $comment->getCompany()->getId()]);
    }
}
