<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\FormErrorForm;
use App\Form\Type\CommentType;
use App\Form\Type\CompanyType;
use App\Form\Type\ComplainType;
use App\Form\Type\LikeType;
use App\Repository\CommentRepository;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Company Controller
 * @Route("/company")
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("/all", name="companies")
     *
     * @param CompanyRepository $companyRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function all(
        CompanyRepository $companyRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $companiesQuery = $companyRepository->getAllActive();
        $companiesPagination = $paginator->paginate($companiesQuery, $request->get('page', 1));
        $formCompany = $this->createForm(CompanyType::class);

        return $this->render('company/all.html.twig', [
            'companies' => $companiesPagination,
            'formCompany' => $formCompany->createView(),
        ]);
    }

    /**
     * @Route("/add", name="company_add")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FormErrorForm $formErrorForm
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function create(
        Request $request,
        EntityManagerInterface $em,
        FormErrorForm $formErrorForm,
        SessionInterface $session
    ) {
        $company = new Company();

        $formCompany = $this->createForm(CompanyType::class, $company);
        $formCompany->handleRequest($request);

        if ($formCompany->isSubmitted() && $formCompany->isValid()) {
            $em->persist($company);
            $em->flush();
            $session->getFlashBag()->add('messages', 'Company successful created and sent to moderation');
        } else {
            $formErrorForm->setOutputError($formCompany);
        }

        return $this->redirectToRoute('companies');
    }

    /**
     * @Route("/search", name="companies_search")
     *
     * @param CompanyRepository $companyRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function search(
        CompanyRepository $companyRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $search = $request->get('search');
        if ($search === null || ($search !== null && strlen($search) < 3)) {
            throw $this->createNotFoundException('Page not exists');
        }

        $companiesQuery = $companyRepository->search($search);
        $companiesPagination = $paginator->paginate($companiesQuery, $request->get('page', 1));
        $formCompany = $this->createForm(CompanyType::class);

        return $this->render('company/search.html.twig', [
            'companies' => $companiesPagination,
            'formCompany' => $formCompany->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="company_one")
     *
     * @param Company $company
     * @param CommentRepository $commentRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function view(
        Company $company,
        CommentRepository $commentRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $commentQuery = $commentRepository->getCommentFirstLevelByCompany($company);
        $commentPagination = $paginator->paginate($commentQuery, $request->get('page', 1));

        $formComment = $this->createForm(CommentType::class)->createView();
        $formLike = $this->createForm(LikeType::class)->createView();
        $formComplain = $this->createForm(ComplainType::class);

        return $this->render('company/view.html.twig', [
            'company' => $company,
            'comments' => $commentPagination,
            'formComment' => $formComment,
            'formLike' => $formLike,
            'formComplain' => $formComplain->createView()
        ]);
    }
}
