<?php

namespace App\Validator;

use App\Entity\Comment;
use App\Entity\Like;
use App\Service\ClientIpService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validation for like ip
 */
class LikeIpValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Like $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        $likes = $this->entityManager->getRepository(Like::class)->findBy([
            'ip' => ClientIpService::getIp(),
            'comment' => $value->getComment()
        ]);

        if (count($likes) !== 0) {
            $this->context
                ->buildViolation('You can put only one like to comment')
                ->addViolation();
        }
    }
}
