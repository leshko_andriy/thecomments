<?php

namespace App\Validator\Constraints;

use App\Validator\CommentIpValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Constraint for comment ip
 * @Annotation
 */
class CommentIpConstraints extends Constraint
{
    /**
     * {@inheritdoc}
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return CommentIpValidator::class;
    }
}
