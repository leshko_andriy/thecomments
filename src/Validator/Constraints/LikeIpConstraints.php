<?php

namespace App\Validator\Constraints;

use App\Validator\LikeIpValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Like for comment ip
 * @Annotation
 */
class LikeIpConstraints extends Constraint
{
    /**
     * {@inheritdoc}
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return LikeIpValidator::class;
    }
}
