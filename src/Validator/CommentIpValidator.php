<?php

namespace App\Validator;

use App\Entity\Comment;
use App\Service\ClientIpService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validation for comment ip
 */
class CommentIpValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Comment $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if ($value->getParent() !== null) {
            return;
        }
        $comments = $this->entityManager->getRepository(Comment::class)->getCommentFirstLevelByIp(
            $value->getCompany(),
            ClientIpService::getIp()
        );

        if (count($comments) >= 3) {
            $this->context
                ->buildViolation('You can write only 3 comments for one company')
                ->atPath('comment')
                ->addViolation();
        }
    }
}
