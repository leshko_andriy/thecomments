<?php

namespace App\EventListener;

use App\Entity\Comment;
use App\Service\CalculateTotalRatingService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Event listener which calculate total rating for company
 */
class CalculateTotalRatingWhenCommentSubscriber implements EventSubscriber
{
    /** @var CalculateTotalRatingService */
    private $calculateTotalRatingService;

    /**
     * @param CalculateTotalRatingService $calculateTotalRatingService
     */
    public function __construct(CalculateTotalRatingService $calculateTotalRatingService)
    {
        $this->calculateTotalRatingService = $calculateTotalRatingService;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postRemove
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event): void
    {
        $entity = $event->getEntity();

        if ($entity instanceof Comment) {
            $company = $entity->getCompany();
            $rating = $this->calculateTotalRatingService->calculate($company);
            $event->getEntityManager()->getUnitOfWork()->scheduleExtraUpdate(
                $entity->getCompany(),
                ['rating' => [null, $rating]]
            );
            $company->setRating($rating);
        }
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postRemove(LifecycleEventArgs $event): void
    {
        $entity = $event->getEntity();

        if ($entity instanceof Comment) {
            $company = $entity->getCompany();
            $rating = $this->calculateTotalRatingService->calculate($company);
            $event->getEntityManager()->getUnitOfWork()->scheduleExtraUpdate(
                $entity,
                ['rating' => [null, $rating]]
            );
            $company->setRating($rating);
        }
    }
}
