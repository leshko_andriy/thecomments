<?php

namespace App\Repository;

use App\Entity\Complain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Complain|null find($id, $lockMode = null, $lockVersion = null)
 * @method Complain|null findOneBy(array $criteria, array $orderBy = null)
 * @method Complain[]    findAll()
 * @method Complain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplainRepository extends ServiceEntityRepository
{
    /**
     * ComplainRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Complain::class);
    }
}
