<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Comment repository
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }
    
    /**
     * @param Company $company
     * @return Query
     */
    public function getCommentFirstLevelByCompany(Company $company): Query
    {
        return $this->createQueryBuilder('c')
            ->where('c.company = :company')
            ->andWhere('c.parent is null')
            ->andWhere('c.active = :active')
            ->setParameters([
                'company' => $company->getId(),
                'active' => true
            ])
            ->orderBy('c.id', 'ASC')
            ->getQuery();
    }

    /**
     * @param Company $company
     * @param string $ip
     * @return array
     */
    public function getCommentFirstLevelByIp(Company $company, string $ip): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.company = :company')
            ->andWhere('c.parent is null')
            ->andWhere('c.ip = :ip')
            ->andWhere('c.active = :active')
            ->setParameters([
                'company' => $company->getId(),
                'ip' => $ip,
                'active' => true
            ])
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
