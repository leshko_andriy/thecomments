<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Company repository
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Company::class);
    }
    
    /**
     * @return Query
     */
    public function getAllActive(): Query
    {
        return $this->createQueryBuilder('c')
            ->where('c.active = :active')
            ->setParameter('active', true)
            ->orderBy('c.id', 'DESC')
            ->getQuery();
    }

    /**
     * @param string $search
     * @return Query
     */
    public function search(string $search): Query
    {
        return $this->createQueryBuilder('c')
            ->where('c.active = :active')
            ->andWhere('c.name like :nameLeft or c.name like :nameCenter or c.name like :nameRight')
            ->setParameters([
                'active' => true,
                'nameLeft' => "%{$search}",
                'nameCenter' => "%{$search}%",
                'nameRight' => "{$search}%",
            ])
            ->orderBy('c.id', 'DESC')
            ->getQuery();
    }
}
