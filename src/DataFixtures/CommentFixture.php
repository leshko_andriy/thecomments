<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Company;
use App\Repository\CompanyRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Demo Comment
 */
class CommentFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            CompanyFixture::class,
        );
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $manager->clear();
        /** @var CompanyRepository $companyRepository */
        $companyRepository = $manager->getRepository(Company::class);

        /** @var Company[] $companies */
        $companies = $companyRepository->findAll();

        foreach ($companies as $company) {
            for ($i = 1; $i <= 10; $i++) {
                $comment = new Comment();
                $comment->setAuthor('Author ' . $i);
                $comment->setEmail('Email ' . $i);
                $comment->setComment('Comment comment comment comment' . $i);
                $comment->setCompany($company);
                $comment->setRating(3);
                $comment->setIp('127.0.0.1');
                $manager->persist($comment);
                if (in_array($i, [1, 2, 4, 9])) {
                    $commentChildren = new Comment();
                    $commentChildren->setAuthor('Author ' . $i);
                    $commentChildren->setEmail('Email ' . $i);
                    $commentChildren->setComment(
                        'Comment Children comment comment comment' . $i
                    );
                    $commentChildren->setCompany($company);
                    $commentChildren->setParent($comment);
                    $commentChildren->setRating(3);
                    $commentChildren->setIp('127.0.0.1');
                    $manager->persist($commentChildren);
                }
            }
        }
        $manager->flush();
    }
}
