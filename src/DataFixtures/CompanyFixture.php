<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Company demo data
 */
class CompanyFixture extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @throws
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $company = new Company();
            $company->setName('Company ' . $i);
            $company->setDescription('Company ' .
                str_repeat('description description description ' ,25) . $i);
            $company->setLocation('Company location ' . $i);
            $company->setActive(true);
            $company->setRating('3');

            $manager->persist($company);
        }
        $manager->flush();
    }
}
