<?php

namespace App\Service;

use App\Entity\Company;

/**
 * Class for calculate total rating
 */
class CalculateTotalRatingService
{
    /**
     * @param Company $company
     * @return float
     */
    public function calculate(Company $company): float
    {
        $comments = $company->getComments();
        $countComment = count($comments);
        $ratingTotal = 0;

        foreach ($comments as $comment) {
            $ratingComment = $comment->getRating();
            $ratingTotal += $ratingComment;
        }

        return ($countComment) ? $ratingTotal / $countComment : 0;
    }
}
